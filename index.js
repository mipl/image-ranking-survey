/**
 * General functions
 */

/**
* Randomize array element order in-place.
* Using Durstenfeld shuffle algorithm.
*/
function shuffleArray(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}

/**
 * Main body
 */

// Get the configuration information
$.getScript("./config.js", function(data, textStatus, jqxhr){
  shuffleArray(source);
  // Create the questions
  var json = populateQuestions(source);
  // Create the survey
  setSurvey(json);
});

/**
 * Creates the questions elements (JSON) format that SurveyJS expects.  Does the shuffling for the questions and the answers too.
 * @param  {json} data The source information for the questions.
 * @return {json} json The final structure for the questions that matches the SurveyJS standard.
 */
function populateQuestions(data) {
  var json = {};
  json.pages = [];
  for (var qk in data) {
    var question = data[qk];
    var q = JSON.parse(JSON.stringify(questionTemplate));
    q.name = q.name+qk;
    var values = [];
    q.choices = [];
    for (var ik in question) {
      item = question[ik];
      var choices = {};
      var href = window.location.href;
      var pathname = href.substring(0, href.lastIndexOf('/')) + "/";
      choices.text = '<image src="' + pathname + item.image + '" alt="' + item.value+ '">';
      choices.value = item.value;
      q.choices.push(choices);
      values.push(item.value);
    }
    shuffleArray(values); // in place
    q.defaultValue = values;
    var page = {};
    page.name = "page" + qk;
    page.elements = [];
    page.elements.push(q);
    json.pages.push(page);
  }
  return json;
}

/**
 * Sends the data to the google form identified with id
 * 
 * @param  {string} id The Google ID for the form we want to submit too.    
 * @param  {json} data The entries data to submit to the form.  It should be a list of pairs with the entry ID and their respective data.
 */
function postGoogleForm(id, data) {
  $.post("https://docs.google.com/forms/d/e/"+id+"/formResponse",
    data,
    function(data, status){
      console.log("Data: " + data + "\nStatus: " + status);
    });
}

/**
 * Collects the answer order from the current page (when the "next" or "complete" button is pressed), and then send it to Google Form.
 * 
 * @param  {Survey.Model} survey Is the model of the survey
 */
function sendAnswers(survey) {
  // Get the answers for the current page
  var answers = Object.values(survey.data)[survey.currentPageNo];
  // Clone the entries of the form
  var formatedEntries = JSON.parse(JSON.stringify(entries));
  // Get the keys to populate the new entries
  var keys = Object.keys(entries)
  for (var i in answers)
    formatedEntries[keys[i]] = answers[i];
  // Send them to Google form
  postGoogleForm(googleUrl, formatedEntries);
}

/**
 * Creates and renders the full survey.  It needs to be deferred since we are loading the config asynchronously.
 * 
 * @param {json} jsonSurvey is the survey data used to render the entire thing
 */
function setSurvey(jsonSurvey) {
var defaultThemeColors = Survey
.StylesManager
.ThemeColors["default"];
defaultThemeColors["$main-color"] = "#555753";
defaultThemeColors["$main-hover-color"] = "#F57900";
// Other colors that can be configured
// defaultThemeColors["$text-color"] = "#4a4a4a";
// defaultThemeColors["$header-color"] = "#555753";

// defaultThemeColors["$background-color"] = "#F8F8F8";

// defaultThemeColors["$header-background-color"] = "#4a4a4a";
// defaultThemeColors["$body-container-background-color"] = "#f8f8f8";

Survey
.StylesManager
.applyTheme();

var survey = new Survey.Model(jsonSurvey);

survey.showTitle = false;
survey.showPageTitles = false; // hide the title for pages
// survey.showQuestionNumbers = false; // check later, but this was giving error :(
survey.showPrevButton = false;

$("#surveyContainer").Survey({
  model: survey,
  onComplete: sendAnswers,
  onCurrentPageChanging: sendAnswers
});

}
