// Fill these with your question groups
var source = [
  [{
    "image": "images/chicken.gif",
    "value": "chicken"
  },
  {
    "image": "images/girl2.gif",
    "value": "girl2"
  },
  {
    "image": "images/girl.gif",
    "value": "girl"
  }],
  [{
    "image": "images/glasses.gif",
    "value": "glasses"
  },
  {
    "image": "images/man.gif",
    "value": "man"
  },
  {
    "image": "images/mario.gif",
    "value": "mario"
  }],
  [{
    "image": "images/wolverine_2.gif",
    "value": "wolverine_2"
  },
  {
    "image": "images/wolverine3.gif",
    "value": "wolverine3"
  },
  {
    "image": "images/wolverine.gif",
    "value": "wolverine"
  }]
];

// You configure these using the "get pre-filled link" option from Google forms
var googleUrl = "1FAIpQLSccIbdA55e80P5pAzuvsyJEBL3SXh-7kM1CNRfZUr7itnhHag";
var entries = { "entry.366340186": "", "entry.1030346785": "", "entry.1845959905": "", "entry.1055521844": ""};


// Template for the sorting questions
var questionTemplate = {
  "type": "sortablelist",
  "name": "sort",
  // You can update this string to change the question
  "title": "Order the elements from more natural to least natural",
  "isRequired": true,
  "useDefaultTheme": false
};