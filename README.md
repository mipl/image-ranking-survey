# About

This is a simple tool that uses JQuery and [SurveyJS](https://surveyjs.io) to generate a set of questions for ranking the images.

You can test this sample at [this project pages](https://mipl.gitlab.io/image-ranking-survey/).

# How to configure it

- Clone this repository on your favorite server.
- You will need a Google form with a set of questions (open questions work the better since they can hold any text).

  Get the "pre-filled link" to get the form ID and the entries IDs too.

  Then you will need to edit the `config.js` file with that information on the `googleUrl` and `entries` variables.

  ```javascript
  var googleUrl = "1FAIpQLSccIbdA55e80P5pAzuvsyJEBL3SXh-7kM1CNRfZUr7itnhHag";
  var entries = { "entry.366340186": "", "entry.1030346785": "", "entry.1845959905": "", "entry.1055521844": ""};
  ```

  On the previous example, the link looks like: `https://docs.google.com/forms/d/e/1FAIpQLSccIbdA55e80P5pAzuvsyJEBL3SXh-7kM1CNRfZUr7itnhHag/viewform?usp=pp_url&entry.366340186=i1&entry.1030346785=i2&entry.1845959905=i3&entry.1055521844=i4`, and you extract the corresponding values from it.

- You can alter the questions by updating the `title` attribute on the `questionTemplate` variable, on the `config.js` too.

- Finally, you need to define your set of images and their corresponding values (those are the strings that will be store on the google form when submitted).  The variable `souce` is an array of arrays, where each array corresponds to the set of images (and their corresponding values) to be shown to the user.

  The images can be local to your `index.hmtl`, and they will be completed when the form is created.  That is, you don't need to add `http://your.domain.com/some/path` to them.